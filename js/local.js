(function() {

  window.AJAXPageLoadInit = function() {
    return $("iframe#directory-listing").removeClass("spinner");
  };
  /*
  Drupal.behaviors.weather = function() {
	  var getWeather = function() {
			var req = $.ajax({
				url:'http://www.shc.edu/weather/weather.json',
				dataType: 'jsonp',
				timeout: 1000 * 5, // 5 seconds
				error: function() { $("#weather").html("weather n/a"); },
				success: function(data) {
					var current = '<div class="current"><a href="http://www.wunderground.com/cgi-bin/findweather/hdfForecast?query=36608&apiref=c7f51e7206b6c23f" title="Weather by Weather Underground">' + data.weather + ' ' + data.temp + '</a></div>';
					$("#weather").html(current);
				}
			});
		};

		doWeather = true;
		if (doWeather) {
			getWeather();
			setInterval(function() {
				getWeather();
			}, 1000 * 60 * 1); // 1 minutes
			//}, 1000 * 5); // 5 seconds
		}
  }*/

  Drupal.behaviors.jCarousel = function(context) {
    $(function() {
      if (typeof mycarousel_itemLoadCallback == 'undefined') {
        return;
      }
      $('.front .home-features-carousel').jcarousel({
          scroll:1,
          auto:6,
          wrap: "last",
          size: mycarousel_itemList.length,
          itemLoadCallback: {onBeforeAnimation: mycarousel_itemLoadCallback},
          initCallback: function (carousel) {
            carousel.clip.hover(function() {
              carousel.stopAuto();
            }, function() {
              carousel.startAuto();
            }); 
          }

      });

  });
    $(".home-features-carousel-nav li.views-row").addClass("hover").each(function(idx) {
      var i = idx + 1;
      $(this).click(function() {
        $('.home-features-carousel').jcarousel('scroll', i);
      })
    })
  };

  Drupal.behaviors.shcSearch = function (context) {
    // if (jQuery().defaultvalue) {
    //   $('.search-box .search-input').defaultvalue(Drupal.t('search shc.edu'));
    // }
  };

  Drupal.behaviors.shcViewRowsEqualheights = function (context) {
    if (jQuery().equalHeights) {
      $(".view-rows-equal-heights li").equalHeights();
    }
  };

  Drupal.behaviors.shcSpinner = function(context) {
    return $("iframe#directory-listing").addClass("spinner");
  };

  Drupal.behaviors.visualSetup = function(context) {
    $(".block-google_cse .form-submit").attr("value", "Go");
    $("table tr:nth-child(odd)").addClass("odd");
    $("table td:first-child, table th:first-child").addClass("first");
    $("table td:last-child, table th:last-child").addClass("last");
    // return $(".page-calendar .view-shc-calendar2 .views-exposed-form select#edit-tid").dropdownchecklist({
    //   zIndex: 1000
    // });
  };

  Drupal.behaviors.weblinksFixes = function(context) {
    var link_cats;
    $('.node-type-weblinks').each(function() {
      var good_href;
      good_href = $(this).find('.weblinks-linkview a').attr('href');
      return $(this).find('h3 a, h2 a').attr({
        'href': good_href
      });
    });
    link_cats = $(".weblinkCat-depth-0");
    if (link_cats.length = 1) {
      link_cats.find('fieldset:first').addClass("undecorated");
    }
    return $('body.logged-in .node-type-weblinks').each(function() {
      var title, _this;
      _this = $(this);
      title = $(this).find('h3.title a').attr('title');
      return _this.find('div.links li.last').removeClass('last').after("<li class=\"show-link last\"> <a class=\"weblinks-ops\" href=\"javascript:alert(\'Copy and paste: \n[link: " + title + "]\')\">Link embed code</a></li>");
    });
  };

  Drupal.behaviors.symlinksFixes = function(context) {
    return $("#pid-academics-majors-and-minors-cs .view-concentrations .view-content a").each(function() {
      return $(this).attr('href', $(this).attr('href').replace('/ug', '/cs'));
    });
  };

  Drupal.behaviors.toc = function(context) {
    var title, _toc;
    _toc = $('#toc');
    title = _toc.attr('title');
    $('#toc').toc({
      'selectors': 'h2,h3',
      'container': 'body #content-content'
    }).addClass('tog_body').before("<h3 id=\"toc-title\" class=\"tog\">" + title + "</h3>");
    $('#toc-title, #toc').wrapAll('<div id="toc-wrapper"></div>');
    return $('#main-content').addClass('toc').find('#content-group-inner').prepend($('#toc-wrapper'));
  };

  Drupal.behaviors.toggler_blocks = function(context) {
    var toggler_blocks;
    toggler_blocks = $(".tog");
    return toggler_blocks.each(function() {
      var togglee;
      togglee = $(this).next(".tog_body").addClass("togglee").hide();
      return $(this).addClass("toggler").attr("title", "click for more info").click(function() {
        togglee.slideToggle('normal');
        return $(this).toggleClass("toggler-open");
      });
    });
  };

}).call(this);
