<div class="carousel-wrapper row">
  <div class="home-features-carousel grid16-16">
    <div class="inner">
      <?php
        print views_embed_view('features2', 'block_1');
      ?>
    </div>
  </div>
  <div class="home-features-carousel-promo block grid16-8">
      <div class="inner">
        <h3 class="brand">Forming leaders engaged in learning, faith, justice and service for life.</h3>
    </div>
  </div>
  <div class="home-features-carousel-nav block grid16-8">
    <div class="inner">
      <?php
        print views_embed_view('features2', 'block_2');
      ?>
    </div>
  </div>
</div>

<!-- main row: width = grid_width -->
<div id="main-wrapper" class="main-wrapper full-width">
<div id="main" class="main row <?php print $grid_width; ?>">
  <div id="main-inner" class="main-inner inner clearfix">

    <div class="row admissions-links">
      <h2 class="hide">Links for future students</h2>
      <?php
        $block = module_invoke('block', 'block', 'view', 15);
        print $block['content'];
      ?>
      <div class="grid16-12 block">
        <h2 class="hide">Featured News Stories</h2>
        <?php
          print views_embed_view('news', 'block_3');
        ?>
      </div>
    </div>

    <div class="row more-news">
      <div class="grid16-16 block"><?php print views_embed_view('news', 'block_4'); ?></div>
    </div>

    <div class="row events">
      <div class="upcoming-events grid16-8 block">
        <h2>Upcoming Events</h2>
        <?php
          print views_embed_view('shc_calendar2', 'block_1');
        ?>
      </div>
                
      <div class="admissions-features grid16-8 block">
        <h2 class="hide">Featured Content</h2>
        <div class="row">
          <a href="/jesuit-advantage">
            <div class="block top">
              <div class="content"><em>The<br>Jesuit</em><br>Advantage</div>
            </div>
          </a>
        </div><!-- /row -->
        <div class="row">
          <a href="/ug/tuition-and-financial-aid">
            <div class="block middle">
              <p class="content">Can I Afford<br><em>SHC?</em></p>
            </div>
          </a>
        </div><!-- /row -->
        <div class="row">
          <a href="/ug/academics/why-liberal-arts">
              <div class="grid16-4 block bottomleft">
                <div class="x-inner">
                  <p class="content">Why<br><em>Liberal Arts?</em></p>
                </div>
              </div>
            </a>
            <a href="/about/gulf-coast">
              <div class="grid16-4 block bottomright">
                <div class="x-inner">
                  <p class="content">Living in<br><em>Mobile</em></p>
                </div>
            </div>
          </a>
        </div><!-- /row -->
      </div><!-- admissions-features -->
    </div><!-- /row -->
  </div><!-- main-inner -->
</div><!-- main -->
</div><!-- main-wrapper -->