<!DOCTYPE html>
<html lang="en-us">

<head>
  <title><?php print $head_title; ?></title>
  <?php print $head; ?>
  <?php print $styles; ?>
  <?php print $setting_styles; ?>
  <!--[if IE 8]>
  <?php print $ie8_styles; ?>
  <![endif]-->
  <!--[if IE 7]>
  <?php print $ie7_styles; ?>
  <![endif]-->
  <!--[if lte IE 6]>
  <?php print $ie6_styles; ?>
  <![endif]-->
  <?php print $local_styles; ?>
  <?php print $scripts; ?>
</head>

<body id="<?php print $body_id; ?>" class="<?php print $body_classes; ?>">
  <div id="page" class="page">


  <div class="test-site-warning">
    <div class="page-inner">
      <h2>Note: this is a test site</h2>
      <p><strong>Please</strong> <a href="/devel/testing-notes">read this very important information about the test site</a>.</p>
    </div>
  </div>
  <div class="ie6 hide">
    <div class="page-inner">
      <p>Your browser is archaic. <a href="http://getfirefox.com">PLEASE upgrade to a modern browser</a>!</p>
    </div>
  </div>

    <div class="frontmatter-wrapper">
      <div id="header-group-wrapper" class="header-group-wrapper">
        <div id="header-group" class="header-group row grid16-16">
          <!-- precursor row: width = grid_width -->
          <?php print theme('grid_row', $precursor, 'precursor', 'full-width', $grid_width); ?>
          <h1 class="header-logo"><a href="/" >Spring Hill College</a></h1>
        </div>
      </div>
      <div class="primary-menu-wrapper">
        <div class="row grid16-16">
          <?php print theme('grid_block', $primary_links_tree, 'primary-menu'); ?>
        </div>
      </div>
    </div>
  
    <div id="page-inner" class="page-inner">
      <div id="skip">
        <a href="#main-content-area"><?php print t('Skip to Main Content Area'); ?></a>
      </div>

      <!-- header-top row: width = grid_width -->
      <?php print theme('grid_row', $header_top, 'header-top', 'full-width', $grid_width); ?>

      <!-- preface-top row: width = grid_width -->
      <?php //print theme('grid_row', $preface_top, 'preface-top', 'full-width', $grid_width);?>

      <?php if (drupal_is_front_page()) { include 'frontpage.tpl.php'; } 
            else { ?>
      
      <!-- main row: width = grid_width -->
      <div id="main-wrapper" class="main-wrapper full-width">
        <div id="main" class="main row <?php print $grid_width; ?>">
          <div id="main-inner" class="main-inner inner clearfix">

            <?php print theme('grid_row', $sidebar_first, 'sidebar-first', 'nested', $sidebar_first_width); ?>

            <!-- main group: width = grid_width - sidebar_first_width -->
            <div id="main-group" class="main-group row nested <?php print $main_group_width; ?>">
              <div id="main-group-inner" class="main-group-inner inner">
                <?php print theme('grid_row', $preface_bottom, 'preface-bottom', 'nested'); ?>

                <div id="main-content" class="main-content row nested">
                  <div id="main-content-inner" class="main-content-inner inner">
                    <!-- content group: width = grid_width - (sidebar_first_width + sidebar_last_width) -->
                    <div id="content-group" class="content-group row nested <?php print $content_group_width; ?>">
                      <div id="content-group-inner" class="content-group-inner inner">
   <?php print theme('grid_block', $breadcrumb, 'breadcrumbs'); ?>
     <?php if ($title): ?>
     <h1 class="title"><?php print $title; ?></h1>
     <?php endif; ?>
                        <?php if ($content_top || $help || $messages): ?>
                        <div id="content-top" class="content-top row nested">
                          <div id="content-top-inner" class="content-top-inner inner">
                            <?php print theme('grid_block', $help, 'content-help'); ?>
                            <?php print theme('grid_block', $messages, 'content-messages'); ?>
                            <?php print $content_top; ?>
                          </div><!-- /content-top-inner -->
                        </div><!-- /content-top -->
                        <?php endif; ?>

                        <div id="content-region" class="content-region row nested">
                          <div id="content-region-inner" class="content-region-inner inner">
                            <a name="main-content-area" id="main-content-area"></a>
                            <?php print theme('grid_block', $tabs, 'content-tabs'); ?>
                            <div id="content-inner" class="content-inner block">
                              <div id="content-inner-inner" class="content-inner-inner inner">
                                    <?php if ($content): ?>
                                <div id="content-content" class="content-content">
                                  <?php print $content; ?>
                                  <?php print $feed_icons; ?>
                                </div><!-- /content-content -->
                                <?php endif; ?>
                              </div><!-- /content-inner-inner -->
                            </div><!-- /content-inner -->
                          </div><!-- /content-region-inner -->
                        </div><!-- /content-region -->

                        <?php print theme('grid_row', $content_bottom, 'content-bottom', 'nested'); ?>
                      </div><!-- /content-group-inner -->
                    </div><!-- /content-group -->

                    <?php print theme('grid_row', $sidebar_last, 'sidebar-last', 'nested', $sidebar_last_width); ?>
                  </div><!-- /main-content-inner -->
                </div><!-- /main-content -->

                <?php print theme('grid_row', $postscript_top, 'postscript-top', 'nested'); ?>
              </div><!-- /main-group-inner -->
            </div><!-- /main-group -->
          </div><!-- /main-inner -->
        </div><!-- /main -->
      </div><!-- /main-wrapper -->
    <?php } ?>

      <!-- postscript-bottom row: width = grid_width -->
      <?php print theme('grid_row', $postscript_bottom, 'postscript-bottom', 'full-width', $grid_width); ?>

    </div><!-- /page-inner -->
  </div><!-- /page -->

  <!-- footer-message row: width = grid_width -->
  <div id="footer-wrapper" class="footer-wrapper full-width">
    <div id="footer" class="footer row grid16-16">

       <div class="grid16-4 block social-links">
        <div class="inner"><?php
            $block = module_invoke('menu', 'block', 'view', 'menu-social-links');
            print $block['content'];
          ?></div>
      </div>
      <div class="grid16-4 block contact-links">
        <div class="inner"><h3>People and Places</h3>
          <?php
            $block = module_invoke('menu_block', 'block', 'view', 5);
            print $block['content'];
          ?></div>
      </div>
       <div class="grid16-4 block services-links">
        <div class="inner">
          <h3>Campus Services</h3>
           <?php
            $block = module_invoke('menu', 'block', 'view', 'menu-services-menu');
            print $block['content'];
          ?>
        </div>
      </div>
      <div class="grid16-4 block other-links">
       <div class="inner">
          <?php
           $block = module_invoke('menu', 'block', 'view', 'menu-footer-links');
           print $block['content'];
         ?>
       </div>
      </div>
      <div class="grid16-16 block" style="margin-top: 40px">
        <p class="physical-address">&copy;Spring Hill College 
          4000 Dauphin St. Mobile, Alabama 36608
          <a class="tel" href="tel:8007426704">800-742-6704</a>
          <a class="tel" href="tel:2513804000">251-380-4000</a>
         </p>
      </div>
      
    
      <!-- footer row: width = grid_width -->
      <?php //print theme('grid_row', $footer, 'footer', 'grid16-16'); ?>

      <!-- <div id="footer-message" class="footer-message inner">
        <?php //print theme('grid_block', $footer_message, 'footer-message-text'); ?>
      </div> --><!-- /footer-message-inner -->

    </div><!-- /footer-message -->
  </div><!-- /footer-message-wrapper -->

  <?php print $closure; ?>
<!-- ON CAMPUS SERVER -->
</body>
</html>
