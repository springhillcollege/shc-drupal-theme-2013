In includes/menu.inc, menu_path_is_external() was hacked to always return TRUE.
        This allows non-drupal relative paths in menus.
        If updated, re-hack to prevent menu path errors.
