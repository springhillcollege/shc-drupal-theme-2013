<item>
	<title><?php print $title; ?></title>
	<link><?php print $link; ?></link>
	<description>
		<?php print $description; ?>
	</description>
	<?php
	if ($image) {
	?>
		<enclosure url="<?php print $image['url']; ?>" length="<?php print $image['filesize']; ?>" type="image/jpeg"/>
	<?php
	}
	?>
	<?php print $item_elements;?>
</item>
