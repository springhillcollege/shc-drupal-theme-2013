<?php
/**
 * @file views-view-list.tpl.php
 * Default simple view template to display a list of rows.
 *
 * - $title : The title of this group of rows.  May be empty.
 * - $options['type'] will either be ul or ol.
 * @ingroup views_templates
 */
?>
<script>
var mycarousel_itemList = [
	<?php for($i = 0; $i <= count($rows) - 1; $i++ ) { 
		$row = str_replace(array("\r", "\n"), '', $rows[$i]); 
		$row = str_replace("'", "&lsquo;", $row);
		?>
      {content: '<?php print $row; ?>'}<?php if ($i != count($rows) - 1) { print ","; } ?>
    <?php }; ?>
];

function mycarousel_itemLoadCallback(carousel, state)
{
	for (var i = carousel.first; i <= carousel.last; i++) {
		if (carousel.has(i)) {
			continue;
		}
		if (i > mycarousel_itemList.length) {
			break;
		}
		carousel.add(i, mycarousel_itemList[i-1].content);
	}
}; 

</script>
<ul></ul>