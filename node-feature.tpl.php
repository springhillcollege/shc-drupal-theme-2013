<?php
// $Id: node.tpl.php 6555 2010-02-24 20:21:15Z chris $
?>
<div class="feature">
	<div class="feature-image">
		<?php if ($node->field_link_to[0]['url']) { ?>
		<a href="<?php print $node->field_link_to[0]['url']; ?>">
		<?php } ?>
    		<?php print $node->field_feature_image[0]['view'] ?>
    	<?php if ($node->field_link_to[0]['url']) { ?>
    	</a>
    	<?php } ?>
	</div>
	<div class="feature-content hide">
		<?php print $node->content['body']['#value']; ?>
	</div>
</div>