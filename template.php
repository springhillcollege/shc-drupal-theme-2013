<?php

function spring_hill_college_menu_item_link($link) {
  if (empty($link['localized_options'])) {
    $link['localized_options'] = array();
  }
  
  if(!isset($link['localized_options']['attributes']['class'])){
    $link['localized_options']['attributes']['class'] = '';
    }
    
    $link['localized_options']['attributes']['class'] .= ' '.title_to_class($link['title']); 

  return l($link['title'], $link['href'], $link['localized_options']);
}


function title_to_class($title){
	$wordlist = array("the", "at", "and", "is", "&amp;", "&" );   
	$title_class = $title;
	foreach($wordlist as $word){
	  $title_class = preg_replace("/\s". $word ."\s/", " ", $title_class);
	  $title_class = preg_replace("/".$word ."\s/", "", $title_class);
	  $title_class = preg_replace("/\s". $word."/", "", $title_class);
	}
	$title_class = str_replace(',', '', $title_class);
	$title_class = filter_var($title_class, FILTER_SANITIZE_STRING, FILTER_FLAG_NO_ENCODE_QUOTES);
	$title_class = str_replace(' ', '-', $title_class);
	$title_class = strtolower($title_class);

	return $title_class;
}


function spring_hill_college_preprocess_views_view_row_rss(&$vars) {
	$view = &$vars['view'];
	$options = &$vars['options'];
	$item = &$vars['row'];

	// Use the [id] of the returned results to determine the nid in [results]
	$result = &$vars['view']->result;
	$id = &$vars['id'];
	$node = node_load( $result[$id-1]->nid );
	
	$vars['title'] = check_plain($item->title);
	$vars['link'] = check_url($item->link);
	//$vars['description'] = check_plain($item->description);
	$vars['description'] = check_plain($node->teaser);
	$vars['node'] = $node;
	$vars['item_elements'] = empty($item->elements) ? '' : format_xml_elements($item->elements);
	
	if ($node->field_images[0]['fid']) {
		$image = array();
		$image_ref = field_file_load($node->field_images[0]['fid']);
		$image['url'] = $GLOBALS['base_url'] . '/' . imagecache_create_path('bio_pic_tn', $image_ref['filepath']);
		$image['filesize'] = $image_ref['filesize'];
		$image['info'] = $image;
		//$image['title'] = $node->content['field_images']['field']['#description'] ? $node->content['field_images']['field']['#description'] : $vars['title'];
		$vars['image'] = $image;
	}

}

?>
